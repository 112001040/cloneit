package com.spring.mongo.demo.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.spring.mongo.demo.model.RedditPost;
import com.spring.mongo.demo.service.MongoApiService;

@RestController
@RequestMapping("/api")
public class MongoQuery {
    private final MongoApiService mongoApiService;

    @Autowired
    public MongoQuery(MongoApiService mongoApiService) {
        this.mongoApiService = mongoApiService;
    }

    @GetMapping("/postTitle/{key}")
    public List<RedditPost> searchPostsByKeyword(@PathVariable String key) {
        String[] keys = key.split("\\+");
        System.out.println(keys);
        List<RedditPost> finalResult=new ArrayList<>();
        for (String keyword : keys) 
        {
            List<RedditPost> result = mongoApiService.searchPostsByKeyword(keyword);
            finalResult.addAll(result);
        }
        if (finalResult.size() == 0)
            throw new PostNotFoundException("Post not found - " + key);

        return finalResult;
    }

    @DeleteMapping("/postId/{postId}")
    public List<RedditPost> deletePost(@PathVariable String postId) {
        RedditPost result = mongoApiService.deletePostByPostId(postId);
        if (result == null)
            throw new PostNotFoundException("Post of postId not found - " + postId);
        List<RedditPost> res = new ArrayList<>();
        res.add(result);
        return res;
    }

    @GetMapping("/userId/{userId}")
    public List<RedditPost> getPostsByUserId(@PathVariable String userId) {
        List<RedditPost> result = mongoApiService.findPostsByUserId(userId);
        if (result.size() == 0)
            throw new PostNotFoundException("Post By userId not found - " + userId);
        return result;

    }
    
    @GetMapping("/postId/{postId}")
    public List<RedditPost> getPostsPostId(@PathVariable String postId) {
        RedditPost result = mongoApiService.findPostsByPostId(postId);
        if (result == null)
            throw new PostNotFoundException("Post By postId not found - "+postId);
        List<RedditPost> res=new ArrayList<>();
        res.add(result);
        return res;

    }

    @GetMapping("/subreddit/{subredditName}")
    public List<RedditPost> getPostsBySubreddit(@PathVariable String subredditName) {
        List<RedditPost> result = mongoApiService.findPostsBySubreddit(subredditName);
        if (result.size() == 0)
            throw new PostNotFoundException("Posts of subreddit not found - " + subredditName);
        return result;

    }

    @GetMapping("/sort")
    public List<RedditPost> getAllPostsSortedByTimestamp() {
        List<RedditPost> result = mongoApiService.getAllPostsSortedByTimestampDescending();
        if (result.size() == 0)
            throw new PostNotFoundException("No posts found ");
        return result;

    }

}
