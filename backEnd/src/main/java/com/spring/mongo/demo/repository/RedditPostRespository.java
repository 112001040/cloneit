
package com.spring.mongo.demo.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.spring.mongo.demo.model.RedditPost;

import java.util.List;

public interface RedditPostRespository extends MongoRepository<RedditPost, String> {

    boolean existsByPostId(String postId);

    List<RedditPost> findByPostTitleIgnoreCaseContaining(String keyword);

    RedditPost deleteByPostId(String postId);

    List<RedditPost> findByUserId(String userId);

    List<RedditPost> findBySubreddit(String subreddit);

    List<RedditPost> findAllByOrderByTimestampDesc(); // Sort in descending order

    RedditPost findByPostId(String postId);
}