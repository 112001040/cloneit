package com.spring.mongo.demo.model;

import java.io.Serializable;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.index.TextIndexed;
import org.springframework.data.mongodb.core.mapping.Document;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Document(collection = "reddit_posts")
public class RedditPost implements Serializable {
    @Id
    private String id;

    @Indexed
    private String postId;
    @TextIndexed
    private String postTitle;
    @Indexed
    private String subreddit;
    @Indexed
    private String userId;
    private long timestamp;

    
    // Getters and setters...
}
