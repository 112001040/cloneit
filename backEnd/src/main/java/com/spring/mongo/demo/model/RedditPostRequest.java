package com.spring.mongo.demo.model;

public class RedditPostRequest {

    private String subreddit;
    private String title;
    private String content;

    // Constructors, getters, and setters

    public RedditPostRequest() {
    }

    public RedditPostRequest(String subreddit, String title, String content) {
        this.subreddit = subreddit;
        this.title = title;
        this.content = content;
    }

    public String getSubreddit() {
        return subreddit;
    }

    public void setSubreddit(String subreddit) {
        this.subreddit = subreddit;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }
}
