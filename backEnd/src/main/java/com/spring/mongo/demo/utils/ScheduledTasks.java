package com.spring.mongo.demo.utils;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.spring.mongo.demo.model.RedditPost;
import com.spring.mongo.demo.repository.RedditPostRespository;
import com.spring.mongo.demo.service.RedditApiService;

@Component
public class ScheduledTasks {

    @Autowired
    private final RedditApiService redditApiService;
    @Autowired
    private RedditPostRespository redditPostRespository;

    @Autowired
    public ScheduledTasks(RedditApiService redditApiService) {
        this.redditApiService = redditApiService;
    }

    @Scheduled(fixedRate = 60000) // Run every 60 seconds
    @PostConstruct
    @Transactional
    public void checkForNewPosts() throws IOException {
        List<RedditPost> newRecentPosts = redditApiService.getRecentPostsFromSubreddits();
        List<RedditPost> toSave = new ArrayList<>();
        for (RedditPost post : newRecentPosts) {
            if (redditPostRespository.existsByPostId(post.getPostId()) == false)
                toSave.add(post);
        }
        System.out.println("******* Inserting Posts to DB *******");
        redditPostRespository.saveAll(toSave);
    }
}
