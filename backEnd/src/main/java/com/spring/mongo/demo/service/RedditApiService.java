package com.spring.mongo.demo.service;


import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.github.jreddit.action.SubmitActions;
import com.github.jreddit.entity.Submission;
import com.github.jreddit.entity.User;
import com.github.jreddit.utils.restclient.HttpRestClient;
import com.github.jreddit.utils.restclient.RestClient;
import com.spring.mongo.demo.model.RedditPost;

import masecla.reddit4j.client.Reddit4J;
import masecla.reddit4j.client.UserAgentBuilder;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.web.client.RestTemplate;

@Service
public class RedditApiService {

    @Value("#{${listOfSubs}}")
    private List<String> subReddList;

    @Value("${reddit.api.url}")
    private String redditApiUrl;

    @Value("${reddit.api.client-id}")
    private String redditApiClientId ;

    @Value("${reddit.api.client-secret}")
    private String redditApiClientSecret ;

    private String redditApiAccessToken;

    @Value("${username}")
    private String username;
    @Value("${password}")
    private String password  ;

    private Reddit4J client;

    // public RedditApiService() {
    //     this.client = Reddit4J.rateLimited().setUsername(username).setPassword(password)
    //             .setClientId(redditApiClientId).setClientSecret(redditApiClientSecret)
    //             .setUserAgent(new UserAgentBuilder().appname("storingPosts").author("shubhang").version("1.0"));
    //     try {
    //         client.connect();
    //     } catch (Exception e) {
    //         e.printStackTrace();
    //         System.out.println("-------------------error in connection----------------");
    //         // TODO: handle exception
    //     }
    // }

    // public void publishPost(String subreddit, String title, String content) {
    //     RestTemplate restTemplate = new RestTemplate();
    //     HttpHeaders headers = new HttpHeaders();
    //     headers.setContentType(MediaType.APPLICATION_JSON);
    //     headers.set("Authorization", "Bearer " + redditApiAccessToken);

    //     String url = redditApiUrl + "/r/" + subreddit + "/submit";
    //     String jsonBody = "{\"kind\": \"self\", \"sr\": \"" + subreddit + "\", \"title\": \"" + title
    //             + "\", \"text\": \"" + content + "\"}";
    //     HttpEntity<String> entity = new HttpEntity<>(jsonBody, headers);
    //     try {
    //         restTemplate.exchange(url, HttpMethod.POST, entity, String.class);

    //     } catch (Exception e) {
    //         e.printStackTrace();
    //         System.out.println("not sent=========================");
    //         // TODO: handle exception
    //     }
    // }

    public List<RedditPost> getRecentPostsFromSubreddit(String subredditName) throws IOException {
        String url = "https://www.reddit.com/r/" + subredditName + "/new.json";
        CloseableHttpClient httpClient = HttpClients.createDefault();
        HttpGet request = new HttpGet(url);
        request.setHeader(HttpHeaders.USER_AGENT,"edge");
        HttpResponse response = httpClient.execute(request);
        String json = EntityUtils.toString(response.getEntity(), StandardCharsets.UTF_8);

        List<RedditPost> recentPosts = new ArrayList<>();
        JSONObject jsonObject = new JSONObject(json);
        JSONArray postArray = jsonObject.getJSONObject("data").getJSONArray("children");
        for (int i = 0; i < postArray.length(); i++) {
            JSONObject postData = postArray.getJSONObject(i).getJSONObject("data");

            String postTitle = postData.getString("title");
            String subreddit = postData.getString("subreddit");
            String userId = postData.getString("author");
            long timestamp = postData.getLong("created_utc");
            String id = postData.getString("id");
            RedditPost redditPost = RedditPost.builder().postId(id)
                    .userId(userId).timestamp(timestamp)
                    .subreddit(subreddit).postTitle(postTitle)
                    .build();
            recentPosts.add(redditPost);
        }
        System.out.println(subredditName);
        return recentPosts;
    }


    public List<RedditPost> getRecentPostsFromSubreddits() throws IOException {
        List<RedditPost> redditPost = new ArrayList<>();
        for (String sub : subReddList) {
            redditPost.addAll(getRecentPostsFromSubreddit(sub));
        }
        return redditPost;
    }

    public List<String> getAllPostsByUser(String username) throws IOException {
        String url = "https://www.reddit.com/user/" + username + "/submitted.json";
        CloseableHttpClient httpClient = HttpClients.createDefault();
        HttpGet request = new HttpGet(url);
        request.setHeader(HttpHeaders.USER_AGENT, "Mozilla/5.0");
        HttpResponse response = httpClient.execute(request);
        String json = EntityUtils.toString(response.getEntity(), StandardCharsets.UTF_8);

        List<String> posts = new ArrayList<>();
        JSONObject jsonObject = new JSONObject(json);
        JSONArray postArray = jsonObject.getJSONObject("data").getJSONArray("children");
        for (int i = 0; i < postArray.length(); i++) {
            JSONObject postData = postArray.getJSONObject(i).getJSONObject("data");
            posts.add(postData.getString("title"));
        }

        return posts;
    }

}
