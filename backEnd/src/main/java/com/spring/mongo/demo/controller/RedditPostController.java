package com.spring.mongo.demo.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.spring.mongo.demo.model.RedditPostRequest;
import com.spring.mongo.demo.service.RedditApiService;

@RestController
public class RedditPostController {

    private final RedditApiService redditApiService;

    @Autowired
    public RedditPostController(RedditApiService redditApiService) {
        this.redditApiService = redditApiService;
    }

    @PostMapping("/api/post")
    @ResponseBody
    public void publishPost(@RequestBody RedditPostRequest postRequest) {
        // System.out.println(postRequest);
        String subreddit = postRequest.getSubreddit();
        String title = postRequest.getTitle();
        String content = postRequest.getContent();
        System.out.println(subreddit+" "+title+" "+content);
        // redditApiService.publishPost(subreddit, title, content);
    }
}
