package com.spring.mongo.demo.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.spring.mongo.demo.model.RedditPost;
import com.spring.mongo.demo.repository.RedditPostRespository;

@Service
public class MongoApiService {
    
    RedditPostRespository redditPostRespository;

    @Autowired
    public MongoApiService(RedditPostRespository redditPostRepository) {
        this.redditPostRespository = redditPostRepository;
    }

    public List<RedditPost> searchPostsByKeyword(String keyword) {
        return redditPostRespository.findByPostTitleIgnoreCaseContaining(keyword);
    }
    
    @Transactional
    public RedditPost deletePostByPostId(String postId) {
        return redditPostRespository.deleteByPostId(postId);
    }

    public List<RedditPost> findPostsByUserId(String userId) {
        return redditPostRespository.findByUserId(userId);
    }
    
    public RedditPost findPostsByPostId(String postId) {
        return redditPostRespository.findByPostId(postId);
    }

    public List<RedditPost> findPostsBySubreddit(String subredditName) {
        return redditPostRespository.findBySubreddit(subredditName);
    }


    public List<RedditPost> getAllPostsSortedByTimestampDescending() {
        return redditPostRespository.findAllByOrderByTimestampDesc();
    }
    
}
