import React, { useState, useEffect, useRef } from "react";
import axios from "axios";

import LinearProgress from "@mui/material/LinearProgress";
import TextField from "@mui/material/TextField";

import { styled } from "@mui/material/styles";
import Box from "@mui/material/Box";
import Button from "@mui/material/Button";
import Typography from "@mui/material/Typography";
import BasicTable from "./basicTable";
import InputLabel from "@mui/material/InputLabel";
import MenuItem from "@mui/material/MenuItem";
import FormControl from "@mui/material/FormControl";
import Select from "@mui/material/Select";
const CssTextField = styled(TextField)({
  "& .MuiOutlinedInput-root": {
    "& fieldset": {
      borderColor: "teal",
    },
    "&:hover fieldset": {
      borderColor: "teal",
    },
    "&.Mui-focused fieldset": {
      borderColor: "teal",
    },
  },
});

const Dashboard = () => {
  const valueRef = useRef("");
  const [userStatusData, setUserStatusData] = useState({});
    const [errorData, setErrorData] = useState({});
  const [requestType, setRequestType] = useState("");
  const [field, setFieldType] = useState("");
  const [key, setKey] = useState("");

  const [formState, setFormState] = useState("IN_PROGRESS");
  const [userRatingsState, setUserRatingsState] = useState("IN_PROGRESS");

  useEffect(() => {
    if (formState === "FETCHING_DATA") {
      if(requestType==="get")
      {
        axios
        .get(`http://localhost:8080/api/${field}/${key}`)
        .then((res) => {
          console.log(res);
          if(res.status===200)
          {
            setFormState("DONE");
            setUserStatusData(res.data);
          }
        })
        .catch((error) => {
        console.log(error.response.data);
          setErrorData(error.response.data);
          setFormState("ERROR");
        });}
        if(requestType==="delete")
      {
          axios
            .delete(`http://localhost:8080/api/${field}/${key}`)
            .then((res) => {
              console.log(res);
              if (res.status === 200) {
                setFormState("DONE");
                setUserStatusData(res.data);
              }
            })
            .catch((error) => {
              console.log(error.response.data);
              setErrorData(error.response.data);
              setFormState("ERROR");
            });
      }
    }
  }, [formState]);



  const handleSubmission = () => {
    setKey(valueRef.current.value);
    if (requestType&&field) {
      setFormState("FETCHING_DATA");
    }
  };


  const handleChangeRequest = (event) => {
    setRequestType(event.target.value);
  };
  const handleChangeField = (event) => {
    setFieldType(event.target.value);
  };
  if (formState === "IN_PROGRESS") {
    return (
      <Box
        component="form"
        sx={{
          "& > :not(style)": { m: 1, width: "25ch" },
        }}
        className="codeforces-id-input"
      >
        <FormControl fullWidth error>
          <InputLabel id="requestType">Request</InputLabel>
          <Select
            labelId="Request"
            id="RequestInput"
            value={requestType}
            label="Request Type"
            onChange={handleChangeRequest}
          >
            <MenuItem value={"get"}>GET</MenuItem>
            <MenuItem value={"delete"}>delete</MenuItem>
          </Select>
        </FormControl>
        <FormControl fullWidth error>
          <InputLabel id="field">field</InputLabel>
          <Select
            
            labelId="field"
            id="fieldType"
            value={field}
            label="field name"
            onChange={handleChangeField}
          >
            <MenuItem value={"postId"}>post Id</MenuItem>
            <MenuItem value={"postTitle"}>post Title</MenuItem>
            <MenuItem value={"userId"}>User Name</MenuItem>
            <MenuItem value={"subreddit"}>subreddit</MenuItem>
            <MenuItem value={"sort"}>Sorted By timestamp</MenuItem>
          </Select>
        </FormControl>
        <CssTextField
          // InputLabelProps={{ style: { color: "grey" } }}
          // InputProps={{ style: { color: "white" } }}
          id="key"
          label="key"
          inputRef={valueRef}
        />
        <Button
          onClick={handleSubmission}
          variant="contained"
          style={{ width: "32ch" }}
        >
          Submit
        </Button>
      </Box>
    );
  } else if (formState === "FETCHING_DATA") {
    return (
      <div className="codeforces-id-input">
        <Typography variant="h5" component="div" color="white" gutterBottom>
          doing {requestType} on {field} and key {key} 
        </Typography>
        <Box sx={{ width: "50%" }}>
          <LinearProgress />
        </Box>
      </div>
    );
  } else if (
    formState === "NO_DATA_FOUND" ||
    formState === "ERROR" ||
    userStatusData === "NO_DATA_FOUND" ||
    userRatingsState === "ERROR"
  ) {
    return (
      <div className="codeforces-id-input" >
        <Typography variant="h4" gutterBottom color="error" component="div">
          {`status: ${errorData.status}`}
        </Typography>
        <Typography variant="h4" gutterBottom color="error" component="div">
          {`message: ${errorData.message}`}
        </Typography>
        <Typography variant="h4" gutterBottom color="error" component="div">
          {`timeStamp: ${errorData.timeStamp}`}
        </Typography>

        <Button
          onClick={() => setFormState("IN_PROGRESS")}
          variant="contained"
          style={{ width: "32ch" }}
        >
          Try Again
        </Button>
      </div>
    );
  }

  return <BasicTable rows={userStatusData} />;
};

export default Dashboard;
