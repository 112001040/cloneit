# CLONE IT

# RUNNING THE APP:

# FOR BACKEND:
*goto backEnd directory*

*mongodb configuration used:*

spring.data.mongodb.host=localhost

spring.data.mongodb.port=27017

spring.data.mongodb.database=Reddit

*edit application.properties for your mongo config*

run ./gradlew build

then run ./gradlew bootRun


# FOR FRONTEND:
goto frontEnd directory

run npm install

then run npm start

# Hope you like it! :)

# Features:
Automatically adds new posts from list of subreddits

operations on mongodb:
*deleting a post by giving postID*

*filtering posts via: postId,subreddit,title containing a keyword,userName*

*retrieve posts sorted by timestamp*
